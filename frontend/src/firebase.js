/**
 * src/firebase.js
 */
import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyC4PT-bqWucAF__HNiNRbYtDGrHcMUThOw",
  authDomain: "devcamp-spaces.firebaseapp.com",
  databaseURL: "https://devcamp-spaces-default-rtdb.firebaseio.com",
  projectId: "devcamp-spaces",
  storageBucket: "devcamp-spaces.appspot.com",
  messagingSenderId: "172157979864",
  appId: "1:172157979864:web:2f92af31131c75b99a5bad"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

export { auth, firebase };
